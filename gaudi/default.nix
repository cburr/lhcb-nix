{ stdenv, fetchurl, fetchpatch, boost, clhep, cmake, cppunit, gperftools
, heppdt, jemalloc, libunwind, python, tbb, utillinux, xercesc, zlib
, ninja, gcc7, overrideCC, root, gdb, aida, gsl, libpng }:

let
    enable_debug_symbols = (l:
        builtins.concatLists (
            builtins.map
            (p: p.all)
            (builtins.map (p: p.overrideAttrs (oldAttrs: rec {
                separateDebugInfo = true;
            })) l)
        )
    );
    custom_stdenv = overrideCC stdenv gcc7;
    root_overridden = root.override { stdenv = custom_stdenv; };
in

custom_stdenv.mkDerivation rec {
  name = "gaudi-${version}";
  version = "v29r0";

  src = fetchurl {
    url = "https://gitlab.cern.ch/gaudi/Gaudi/repository/${version}/archive.tar.gz";
    sha256 = "1ijdq1l8rscwij9hgyzrlvga1qg7b0csx76wcd76x3yli8bc766b";
  };

  buildInputs = (builtins.concatLists [
    [ cmake python gdb aida ninja ]
    [ (root_overridden.override { python = python; cxx_standard = "cxx14"; }).out ]
    (enable_debug_symbols (
        builtins.map
        (p: p.override { stdenv = custom_stdenv; })
        [
          boost
          clhep
          cppunit
          gperftools
          heppdt
          jemalloc
          libunwind
          tbb
          utillinux
          xercesc
          zlib
          gsl
          libpng
        ]
    ))
  ]);

  patches = [ ./fix-profiling.patch ];

  cmakeFlags = [
    "-GNinja"
  ];

  enableParallelBuilding = true;

  meta = {
    homepage = https://root.cern.ch/;
    description = "A data analysis framework";
    platforms = stdenv.lib.platforms.unix;
    maintainers = with stdenv.lib.maintainers; [ chrisburr ];
  };
}
