{ nixpkgs ? { outPath = <nixpkgs>; revCount = 1234; shortRev = "abcdef"; }
, name ? "user_environment"
, compiler ? "gcc6"
, root_python ? "python27"
, cxx_standard ? "cxx14"
, extra_packages ? []
}:

with import <nixpkgs> {};

# TODO Consider using
# https://github.com/NixOS/nixpkgs/blob/fe2e7def5341b0f5fe4661ae9853756600d4c384/pkgs/stdenv/adapters.nix#L228
let
    enable_debug_symbols = (l:
        builtins.concatLists (
            builtins.map
            (p: p.all)
            (builtins.map (p: p.overrideAttrs (oldAttrs: rec {
                separateDebugInfo = true;
            })) l)
        )
    );
    gcc = pkgs.${compiler};
    custom_stdenv = pkgs.overrideCC pkgs.stdenv gcc;
    root = pkgs.root.override { stdenv = custom_stdenv; };
in

let
    cpp_packages = [
        pkgs.boost
        pkgs.gsl
        pkgs.xrootd
        pkgs.libxml2
        pkgs.pcre
        pkgs.pcre2
        pkgs.sqlite
        pkgs.fftw
        pkgs.libpng
        pkgs.libjpeg
        pkgs.libsodium
        pkgs.tbb
        pkgs.cppunit
        pkgs.xercesc
        pkgs.clhep
        pkgs.heppdt
        pkgs.libunwind
        pkgs.gperftools
        pkgs.utillinux
        pkgs.zlib
        pkgs.jemalloc
    ];

    python2and3_packages = [
        "cycler"
        "dateutil"
        "ipython"
        "matplotlib"
        "mock"
        "nose"
        "pandas"
        "pycairo"
        "pyparsing"
        "pytz"
        "scipy"
        "six"
        "sphinx"
        "tornado"
        "flake8"
        "pep8"
        "pep257"
        "joblib"
        "gevent"
        "pyparsing"
        "paramiko"
        "pytest"
        "pytz"
        "simplejson"
        "markupsafe"
        "lxml"
        "nose"
        "jupyter"
        "jug"
        "pyfftw"
    ];

    misc_packages = [
        # ROOT has it's stdenv overridden manually
        (root.override { python = pkgs.${root_python}; cxx_standard = cxx_standard; }).out
        # Debuggers
        pkgs.gdb
        pkgs.lldb
        pkgs.valgrind
        # Standard unix utilities
        pkgs.gnugrep
        pkgs.gnumake
        pkgs.gnused
        pkgs.gnutar
        pkgs.findutils
        pkgs.less
        pkgs.man
        pkgs.gzip
        pkgs.tree
        pkgs.which
        pkgs.curlFull
        pkgs.rsync
        pkgs.wget
        pkgs.coreutils
        pkgs.htop
        pkgs.openssh_with_kerberos
        pkgs.openssl
        pkgs.patchelf
        pkgs.texlive.combined.scheme-full
        pkgs.ghostscript
        pkgs.aida
        pkgs.nettools
        pkgs.bind
        pkgs.gawk
        pkgs.psmisc
        pkgs.diffutils
        pkgs.colordiff
        pkgs.bzip2
        pkgs.procps
        pkgs.gnupg
        pkgs.mawk
        pkgs.nawk
        pkgs.ncurses
        pkgs.strace
        pkgs.ltrace
        pkgs.imagemagick7Big
        # Text editors
        pkgs.nano
        pkgs.vim
        pkgs.neovim
        pkgs.atom
        # Build utilities
        pkgs.ninja
        pkgs.cmake
        # Shells
        pkgs.bashInteractive
        pkgs.bash-completion
        pkgs.zsh
        pkgs.zsh-completions
        pkgs.nix-zsh-completions
        pkgs.zsh-syntax-highlighting
        pkgs.tcsh
        pkgs.dash
        # Other programming languages
        pkgs.jdk8
        pkgs.php
        pkgs.perl
        (wrapCC (gcc.cc.override {
            name = "gfortran";
            langFortran = true;
            langCC = false;
            langC = false;
            profiledCompiler = false;
        }))
        # Version control
        pkgs.git
        pkgs.subversion
        pkgs.mercurial
        # Miscellaneous
        pkgs.krb5Full
        pkgs.graphviz
        pkgs.pandoc
    ];

    nix_packages = [
        pkgs.nix
        pkgs.nix-repl
        pkgs.nox
        pkgs.pypi2nix
    ];
in
    with {
        cpp_packages_with_stdenv = enable_debug_symbols (
            builtins.map
            (p: p.override { stdenv = custom_stdenv; })
            cpp_packages
        );
    };

let
    user_environemnt = (buildEnv {
      name = name;
      paths = (builtins.concatLists [
        [ gcc ]
        nix_packages
        misc_packages

        [
            # # Python 2.7
            (root.override { python = python27; }).pythonlib
            ((pkgs.python27Full.withPackages(ps:
                (builtins.map (s: ps.${s}) python2and3_packages)
             )).override {
                # Workaround https://github.com/NixOS/nixpkgs/issues/22319
                ignoreCollisions = true;
            })

            # Python 3.6
            (root.override { python = python36; }).pythonlib
            (pkgs.python36Full.withPackages(ps: (builtins.concatLists [
                    (builtins.map (s: ps.${s}) python2and3_packages)
                    [ ps.snakemake ]
                ])
            ))
        ]

        cpp_packages_with_stdenv
        extra_packages
      ]);
    });
in user_environemnt

