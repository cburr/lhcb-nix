{ nixpkgs }:

with import <nixpkgs> {};

let
    jobs = {
        gaudi = callPackage ./gaudi {};
    };
in jobs
