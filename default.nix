{ nixpkgs }:

with import <nixpkgs> {};

let
    jobs = {
        # Some example environments for now
        example_environment_gcc6 = callPackage ./make_user_environment.nix {
            name = "analysis_environment_gcc6";
            nixpkgs = nixpkgs;
            compiler = "gcc6";
        };
        example_environment_gcc7 = callPackage ./make_user_environment.nix {
            name = "analysis_environment_gcc7";
            nixpkgs = nixpkgs;
            compiler = "gcc7";
        };
        gaudi_environment_gcc7 = callPackage ./make_user_environment.nix {
            name = "gaudi_environment_gcc7";
            nixpkgs = nixpkgs;
            compiler = "gcc7";
            extra_packages = [ (callPackage ./gaudi {}) ];
        };

        # Make a replacement nixpkgs channel
        lhcb_nixpkgs = pkgs.releaseTools.channel {
            constituents = [];
            name = "lhcb_nixpkgs";
            src = pkgs.path;
            isNixOS = false;
        };
    };
in jobs
