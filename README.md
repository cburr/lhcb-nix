# Nix recipes for LHCb

## Useful links

- [nix manual](https://nixos.org/nix/manual/)
- [nixpkgs manual](https://nixos.org/nixpkgs/manual/)

## Installing a test in docker without cvmfs mounted

This section runs over installing on a dummy cvmfs folder in a docker container created using:

```bash
docker run --rm -it centos:7 bash
```

### Installing nix

```bash
useradd test
yum install -y bzip2
mkdir -p -m 0755 /cvmfs/lhcbdev.cern.ch/nix/v0.1
chown test /cvmfs/lhcbdev.cern.ch/nix/v0.1
su test bash -c "curl https://gitlab.cern.ch/cburr/lhcb-nix/raw/master/install.sh | sh"
```

The custom channels created using [the hydra instance](http://lhcb-hydra-nix.cern.ch:3000) are added automatically.

### Installing environments

To see the list of available environments:

```bash
su test
. /home/test/.nix-profile/etc/profile.d/nix.sh
export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8

nix-env -qa -f ~/.nix-defexpr/channels/lhcb-nix-master/
```

To install the example gcc7 environment in `~/.nix-profile`:

```bash
su test
. /home/test/.nix-profile/etc/profile.d/nix.sh
export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8

nix-env -ir analysis_environment_gcc7 -Q -j8
export LHCB_NIX_ENV_DIR="${HOME}/.nix-profile"
export CMAKE_PREFIX_PATH="${LHCB_NIX_ENV_DIR}"
```


### Install example environments on cvmfs

```bash
mkdir -p "/cvmfs/lhcbdev.cern.ch/nix/v0.1/environments/"

export LHCB_NIX_ENV_DIR="/cvmfs/lhcbdev.cern.ch/nix/v0.1/environments/analysis_environment_gcc7"
nix-env -ir analysis_environment_gcc7 --profile "${LHCB_NIX_ENV_DIR}" -Q -j8

export LHCB_NIX_ENV_DIR="/cvmfs/lhcbdev.cern.ch/nix/v0.1/environments/analysis_environment_gcc6"
nix-env -ir analysis_environment_gcc6 --profile "${LHCB_NIX_ENV_DIR}" -Q -j8

export LHCB_NIX_ENV_DIR="/cvmfs/lhcbdev.cern.ch/nix/v0.1/environments/gaudi_environment_gcc7"
nix-env -ir gaudi_environment_gcc7 --profile "${LHCB_NIX_ENV_DIR}" -Q -j8
```

### Using cvmfs environments

The environment can then be set up by any user, for example to get the example gaudi environment:

```bash
export LHCB_NIX_ENV_DIR="/cvmfs/lhcbdev.cern.ch/nix/v0.1/environments/gaudi_environment_gcc7"
export PATH="${LHCB_NIX_ENV_DIR}/bin"
export CMAKE_PREFIX_PATH="${LHCB_NIX_ENV_DIR}"
export NIX_SSL_CERT_FILE=/etc/ssl/certs/ca-bundle.crt
bash
```
