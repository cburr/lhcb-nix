#!/usr/bin/env bash
oops() {
    echo "$0:" "$@" >&2
    # exit 1
}

tmpDir="$(mktemp -d -t nix-binary-tarball-unpack.XXXXXXXXXX || \
          oops "Can\'t create temporary directory for downloading the Nix binary tarball")"
cleanup() {
    rm -rf "$tmpDir"
}
trap cleanup EXIT INT QUIT TERM

require_util() {
    type "$1" > /dev/null 2>&1 || which "$1" > /dev/null 2>&1 ||
        oops "you do not have '$1' installed, which I need to $2"
}

url="http://lhcb-hydra-nix.cern.ch:3000/job/nix/testing/binaryTarball.x86_64-linux/latest/download-by-type/file/binary-dist"
tarball="$tmpDir/nix-x86_64-linux.tar.bz2"

require_util curl "download the binary tarball"
require_util bzcat "decompress the binary tarball"
require_util tar "unpack the binary tarball"

echo "downloading Nix binary tarball for $system from '$url' to '$tmpDir'..."
curl -L "$url" -o "$tarball" || oops "failed to download '$url'"

unpack=$tmpDir/unpack
mkdir -p "$unpack"
< "$tarball" bzcat | tar x -C "$unpack" || oops "failed to unpack '$url'"

script=$(echo "$unpack"/*/install)
echo "Running script at $script"
[ -e "$script" ] || oops "Installation script is missing from the binary tarball"
"$script"

. /home/test/.nix-profile/etc/profile.d/nix.sh
export LC_ALL=en_US.utf-8
export LANG=en_US.utf-8

nix-channel --add http://lhcb-hydra-nix.cern.ch:3000/channel/custom/nixpkgs/lhcb-nix-master/lhcb_nixpkgs nixpkgs
nix-channel --add http://lhcb-hydra-nix.cern.ch:3000/jobset/nixpkgs/lhcb-nix-master/channel/latest lhcb-nix-master
nix-channel --update
